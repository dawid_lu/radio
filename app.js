
function consoleLog(button){
	console.log("User clicked "+button+" button");
};

var app = angular.module("myApp",[]);
app.controller("myCtrl",function($scope){
	$scope.station = [	
	{radioStation: "Putin", frequency: "66,6", img: "imgs/putin.jpg"},
	{radioStation: "Pribble", frequency: "101,2", img: "imgs/pribble.jpg"},
	{radioStation: "Doge", frequency: "99,4", img: "imgs/doge.jpg"},
	{radioStation: "Ballads", frequency: "87,1", img: "imgs/ballads.jpg"},
	{radioStation: "Maximum", frequency: "142,2", img: "imgs/maximum.jpg"}
	]
	var  previousStation
	$scope.setStation = function(stationName) {
		document.getElementById("footer").innerHTML = "<span style='font-size:12px;color:rgb(225,165,93);'>CURRENTLY PLAYING<br></span>" + stationName +" FM";
		document.getElementById(stationName).style.display = "block";
		if (previousStation != null && previousStation != stationName) {
		document.getElementById(previousStation).style.display = "none";
		}
		previousStation = stationName;
};
});